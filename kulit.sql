/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.5.5-10.1.21-MariaDB : Database - kulit
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`kulit` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `kulit`;

/*Table structure for table `analisa_hasil` */

DROP TABLE IF EXISTS `analisa_hasil`;

CREATE TABLE `analisa_hasil` (
  `id` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama` varchar(60) NOT NULL,
  `kelamin` enum('P','W') NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `pekerjaan` varchar(60) NOT NULL,
  `kd_penyakit` char(4) NOT NULL,
  `kodeauto` varchar(60) NOT NULL,
  `tanggal` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `analisa_hasil` */

insert  into `analisa_hasil`(`id`,`nama`,`kelamin`,`alamat`,`pekerjaan`,`kd_penyakit`,`kodeauto`,`tanggal`) values (0001,'E','P','E','E','P009','7ILRSIkxVa','2018-05-21 05:01:44'),(0002,'F','P','F','F','P006','dMdE9UegmM','2018-05-21 05:10:43'),(0003,'H','P','H','H','P006','AWlNK6wtm8','2018-05-21 05:12:59'),(0004,'Popo','P','jakarta timur','swasta','P004','gFJNgR2Tj7','2018-05-21 17:47:44');

/*Table structure for table `gejala` */

DROP TABLE IF EXISTS `gejala`;

CREATE TABLE `gejala` (
  `kd_gejala` char(4) NOT NULL DEFAULT '',
  `nm_gejala` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kd_gejala`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `gejala` */

insert  into `gejala`(`kd_gejala`,`nm_gejala`) values ('G001','Adanya rasa gatal'),('G002','Adanya elemen jamur'),('G003','Adanya sisik halus'),('G004','Adanya sisik kasar'),('G005','Sisik menyerupai lingkaran bermata 1'),('G006','Adanya perubahan pada warna kulit'),('G007','Kulit berwarna merah kehitaman'),('G008','Adanya bintik-bintik kemerahan'),('G009','Timbulnya bintik-bintik berwarna merah kuning'),('G010','Timbulnya bintik hitam kecoklatan'),('G011','Adanya butiran-butiran kuning kehijauan'),('G012','Adanya rasa pedih pada kulit'),('G013','Kulit panas seperti terbakar'),('G014','Adanya rasa nyeri otot'),('G015','Adanya skuama'),('G016','Skuama lebih tebal dan berlapis lapis'),('G017','Timbulnya lesi'),('G018','Lesi menyerupai kembang kol'),('G019','Adanya pembengkakkan lesi'),('G020','Adanya pembengkakan kulit'),('G021','Adanya nanah'),('G022','Adanya edema'),('G023','Adanya Postula'),('G024','Adanya Vesikopustula miliar'),('G025','Adanya vesikel'),('G026','Vesikula miliar dan dalam'),('G027','Adanya tumor/kutil'),('G028','Adanya kelainan bentuk pada kaki'),('G029','Adanya pembengkakan pembuluh limfe'),('G030','Adanya jembatan-jembatan kutil'),('G031','Adanya fisura pada jari'),('G032','Rambut mudah putus'),('G033','Warna rambut kusam'),('G034','Berbau busuk'),('G035','Tampak bisul-bisul kecil pada kulit kepala'),('G036','Kuku berwarna hitam coklat tak bercahaya'),('G037','Fistel mengeluarkan eksudat keputi-pitihan'),('G038','Tampak reaksi radang pada folikel'),('G039','Timbulnya hiperkeratotik/hiperkeratosis'),('G040','Timbul fistel-fistel'),('G041','Timbul papula/Nodula'),('G042','Terjadinya eritema'),('G043','Timbulnya ulkus');

/*Table structure for table `pakar` */

DROP TABLE IF EXISTS `pakar`;

CREATE TABLE `pakar` (
  `userID` varchar(50) NOT NULL DEFAULT '',
  `passID` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `pakar` */

insert  into `pakar`(`userID`,`passID`) values ('admin','admin');

/*Table structure for table `penyakit` */

DROP TABLE IF EXISTS `penyakit`;

CREATE TABLE `penyakit` (
  `kd_penyakit` char(4) NOT NULL,
  `nm_penyakit` varchar(60) NOT NULL,
  `penyebab` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `photo` text NOT NULL,
  `solusi` text NOT NULL,
  PRIMARY KEY (`kd_penyakit`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `penyakit` */

insert  into `penyakit`(`kd_penyakit`,`nm_penyakit`,`penyebab`,`keterangan`,`photo`,`solusi`) values ('P001','Tinea Kruris (Ekzema Marginatum)','Disebabkan Oleh Jamur E.floccosum, T.rubrum dan T.mentagrophytes','Tinea dari selangkangan (\"gatal joki\") cenderung mempunyai suatu warna coklat kemerahan dan meluas dari lipatan-lipatan selangkangan turun ke satu atau kedua paha. Kondisi-kondisi lain yang dapat meniru tinea cruris termasuk infeksi-infeksi ragi, psoriasis, dan intertrigo, suatu ruam gossokan (chafing rash) yang berakibat dari gosokkan kulit terhadap kulit.','tinea_cruris.jpg','Menghilangkan faktor predisposisi, menganjurkan pasien mengusahakan\r\ndaerah lesi selalu kering dan memakai pakaian yang menyerap keringat.\r\nBila menggunakan terapi topikal, pengobatan dilanjutkan hingga 1 minggu\r\nsetelah lesi sembuh.\r\nJika lesi luas atau gagal dengan terapi topikal, dapat digunakan obat oral\r\nseperti griseofulvin 500-1000 mg/hari (dewasa) atau 10-20 mg/kgBB/hari\r\n(anak-anak) dosis tunggal selama 2-6 minggu atau terbinafin 250 mg/hari\r\n(dewasa) selama 1- 2 minggu atau itrakonazol 2x100 mg/hr selama 2 minggu\r\natau ketokonazol 200 mg/hr selama 10-14 hari.\r\n'),('P002','Tinea Kapitis','Disebabkan Oleh Jamur Golongan Dermatofita, terutama T.rubrum, T.Mentagropytes dan M.gypseum','Tinea kapitis (tinea capitis) adalah infeksi jamur pada folikel rambut kulit kepala yang ditandai oleh pembentukan kerak kecil di dasar folikel. Kondisi ini juga disebut kurap kulit kepala. \r\n. Penyakit ini sering terjadi pada anak-anak yang dapat ditularkan dari binatang peliharaan misalnya kucing dan anjing. Selain itu lingkungan kotor dan panas serta udara yang lembap ikut berperan dalam penularan penyakit ini.\r\n','kapitis.jpeg','pada anak biasanya diberikan per oral dengan griseofulvin 10-25 mg/kg berat badan per hari selama 6 minggu. Dosis pada orang dewasa sebesar 500 mg perhari selama 6 minggu. Selain itu pengobatan dapat dilakukan dengan mencuci kepala dan rambut dengan shampo desinfektan antimikotik seperti larutan asam'),('P003','Tinea Manus','Disebabkan Oleh Jamur T.mentagropytes dan T.rubrum','Tinea manus: Ringworm melibatkan tangan-tangan, terutama telapak tangan dan ruang-ruang antara jari-jari tangan. Ia secara khas menyebabkan penebalan (hyperkeratosis) dari area-area ini, seringkali pada hanya satu tangan. Tinea manus adalah suatu teman yang umum dari tinea pedis (ringworm dari kaki-kaki). Ia juga disebut tinea manuum.','Tinea_manus1.jpg','Pemilihan terapi topikal atau sistemik antara lain bergantung pada luas lesi dan ada/tidaknya kontraindikasi. \r\nPreparat topical yang dapat digunakan antara lain golongan imidazol atau alilamin.\r\nObat topikal digunakan hingga 1 minggu setelah lesi sembuh.\r\n'),('P004','Tinea Versikolor (Panu)','Disebabkan Oleh Malassezia Furfur','Tinea Vesikolor adalah suatu penyakit jamur kulit yang kronik dan asimtomatik serta ditandai dengan bercak putih sampai coklat yang bersisik. Kelainan ini umumnya menyerang badan dan kadang-kadang terlihat di ketiak, sela paha, tungkai atas, leher, muka, dan kulit kepala.','panu.jpg','salaep whitfield,salep salsil sulfur, larutan salisil spritus, larutan tiosulfat natrikus dan latio kumerfeldi.'),('P005','Tinea Imbrikata','Disebabkan oleh Jamur Tichophyton Concentricum','Infeksi jamur superfisial yang menyerang kulit dengan gambaran khas berupa skuama kasar yang tersusun konsentris sehingga tanpak seperti atap genteng.','imbrikata.jpg','Sistemik Griseofulvin 0,5 g selama l-2bulan.\r\nTopikal Keratolitik kuat yang bersifat fungisid antara lain:\r\nkrisarobin 5%, sulfur 5% atau asam salisilat 5%. G Castellani, spaint G Salep Whitfield 2 kali sehari G Antimikotik golongan imidazol mempunyai khasiat baik.\r\n'),('P006','Tinea Barbae & Sikosis Barbae','Disebabkan Oleh Jamur Golongan Trichopyton dan Microsporum','Adalah bentuk infeksi jamur dermatofita pada dacrah dagu, jenggot, yang menyerang kulit dan folikel rambut.\r\nYang terkene jamur ini akan mudah patah, kusam. Sedangkan kulit bagian leher yang terkene virus ini akan terdapat bintik-bintik putih.\r\nDi sebabkan oleh Jamur Trichophyton mentagrophytes, Trichophyton violaceum, Microsporum cranis. Dapat tertular apabila bersentuhan atau melalui alat pemotong jenggot yang telah terinfeksi\r\n','barbae.jpg','Toksilat, haloprogin, tolnaftate, dan derival imidasol seperti mikonasol, ekonasol,bifanasol, kotrimasol dalam bentuk larutan atau krem dengan konsentrasi 1-2% dioleskan 2x sehari dalam waktu 1-3 minggu'),('P007','Tinea Nigra Palmaris','Disebabkan Oleh Jamur Cladosporium Werneckii','Tinea nigra palmaris adalah penyakit infeksi jamur superfisial yang menyerang telapak kaki dan tangan, menimbulkan gambaran khas berupa warna coklat-kehitamanpada kulit.','palmaris.jpg','salep anti jamur seperti Salep whitfield 1 dan 2 atau salep sulfur salisil\r\nObat anti jamuur dan preparat-preparat imidazol, seperti isokotonasol, bifonasol, klotrimasol dalam bentuk salep atau krim\r\n'),('P008','Kandidiasis','Disebabkan Oleh Jamur Candida albicans','Adalah penyakit infeksi yang disebabkan oleh jamur kandida /candida albicans. merupakan jamur mirip ragi dan selalu ada dalam tubuh kita, dalam jumlah sedikit. Dalam keadaan normal jamur ini hidup di rongga mulut, vagina dan usus. Tanpa menimbulkan gangguan atau penyakit.\r\nPerubahan kondisi tubuh kadang bisa mengakibatkan kandida tumbuh dan berkembang biak secara berlebihan sehingga menimbulkan infeksi yang di sebut Kandidiasis jadi.. “dia” termasuk penyakit yang bersifat oportunistik dalam keadaan normal jamur ini tidak menyebabkan ganguan. Na..muunn..bila kulit lembab, hangat, terluka, akan memicu Kandida semakin memeperbanyak diri dan akhirnya membuat infeksi.\r\nInfeksi kandida menyerang kulit dan selaput mukosa; yaitu jaringan yang melapisi permukaan bagian tubuh kita misalnya vagina, (vagina condidiasis) yang sering kita kenal keputihan dan rongga mulut, (oral condidiasis) Kandida juga merupakan penyebab tersering dari paronokia  yaitu infeksi kulit di sekitar kuku dan onikomikosis panyakit jamur kuku.\r\n','KANDIDIASIS.jpg','larutan gentian violet 1-2% dioleskan 1-2 kali sehari sampai dengan 5-7 hari, biasanya dipakai untuk kandidasis kuitis dan mukokutan\r\nNistatin\r\n- Bentuk krim atau salep dipakai untuk mengobati kandidiasis kuitis\r\n- Bentuk larutan dipakai untuk  kuku dan mult\r\n-bentuk tablet untuk mengatasi kandidasis disaluran cerna\r\n- bentuk tablet vaginal untuk mengatasi kandidiasis vaginitis\r\nAmfoterisin B\r\n- bentuk salep atau krim untuk mengobati kandidiasis kulit\r\n- Bentuk tablet untuk mengatasi atau memberantas sumber infeksi di dalam usus\r\n- Bentuk tablet Vaginal untuk mengatasi vaginitis\r\n-Bentuk suntikan untuk mengobati kandidiasis sistemik\r\nNatasin\r\n- Bentuk salep atau krim untuk mengobati kandidiasis kulit\r\n- Bentuk tablet vagina mengobati vulvovaginitis\r\nTrikomisin, bentuk salep atau krim untuk mengobati kandidiasis kutis dan selaput lendir\r\n5 Fluorositosin (5F.C), bentuk tablet untuk mengobati kandidiasis alat cerna dan kandidiasis sitemik\r\nAsam undesilinat, dalam bentuk salep 5%, krim 5%, dan tingtura 5% serta dalam bentuk larutan 15% dapat mengobati kandidiasis kuitis dan selaput lendir.\r\n'),('P009','Misetoma','Disebabkan Oleh Jamur Actinomycetes termasuk genus Nocardia dan Streptomyces','Merupakan infeksi kronik pada jaringan dibawah kulit, yang dapat melua sampai ke fasia dan tulang-tulang dengan menimbulkan kelainan-kelainan berupa pembengkakan kruris deforminitas dari alat-alat yang diserang\r\nDaerah yang diserang adalah telapak kaki, tangan, pergelangan kaki, tangan, dan lutut\r\n','mesitoma.jpg','Untuk jenis misetoma aktinomikotik dapat diobati dengan penisilin prokain dosis tinggi 2.4 juta unit sampai 2 mega unit. Etiologinya genus nokardia yang diobati dengan preparat sulfur, seperti sulfadiasin 3-8 gram perhari diberikan sampai 2-4 minggu\r\nPengobatan misetoma eumikotik sangat susah susah dan dapat dicoba dengan amfoterisin B intravena. Pengobatan dengan turunan Asol, seperti itrakonasol 400 mg/hari dan flutosin 2gr/hari selama 1-2 bulan.\r\n'),('P010','Kromomikosis','Disebabkan Oleh Jamur Phialophora pedrosoi, P.verrucosa, P.compacta dan Cladosporium carioni','Merupakan penyakit jamur yang biasanya mengenai kaki, tangan, dan bokong. Penyebab kromomikosis masuk kedalam tubuh melalui luka- dikulit, secara kontak langsung.\r\nPenyakit ini banyak menyerang pria, terutama petani dan buruh peternakan. Biasanya dimulai dengan pembengkakkan berupa pembentukan nodul-nodul pada tempat luka atau aberasi kulit\r\n','kromi.jpg','Amfoterisin B dosisi tinggi dapat diberikan suntikan didalam lesi\r\nlarutan kalium iodida\r\nDerifatasol, itrakonasol dengan dosis 2x100 mg/hari selama 5 bulan.\r\n'),('P011','Sporotrikosis','Disebabkan Oleh Jamur Sporotrichum schenkii','Merupakan penyakit jamur kronik yang disebabkan  oleh sporothrix schenchii, yaitu suatu jamur yang bersifat dirmorfik. jamur dapat masuk ke dalam tubuh memaluli luka-luka akibat kecelakaan atau luka dikulit akibat garukan. Manusia dapat ditulari dari binatang, seperti kuda, kucing, anjing, atau tikus.','sporotrichosis-on-the-forearm.jpg','Larutan kalium iodida jenuh dapat memberikan hasil yang memuaskan. dengan dosis 30-50 tetes 3 kali sehari selama 1-2 bulan memberi hasil yang memuaskan. pemberian dimulai dengan 5 tetes 3 kali sehari dan dosis tiap minggu dinaikan sampai mencapai 30-50 tetes 3 kali sehari.\r\nDengan adanya obat antimikosis yang baru seperti ketokonasol dengan dosis 2x100 mg/hari selama 2-4 minggu atau itrakonasol 2x100mg/hari selama 1-2 bulan memberikan hasil yang memuaskan\r\nPengobatan lokal dapat dikompres dengan larutan lugol sampai lesi-lesi menutup\r\nObat anti biotik lain, seperti amfoterisin B diberikan dengan intravena dengan dosis 0.25 mg/kg berat badan dapat memberikan hasil yang baik. dosis maksimum adalah 1 mg/kg berat badan\r\n'),('P013','Aktinomikosis','Disebabkan Oleh Jamur Actinomyces israelii','Adalah penyakit jamur kronis berupa lesi supuratif granulomatosa superfisial atau viseral yang timbulnya dari pemecahan abses-abses sehingga menimbulkan fistel-fistel yang multipel\r\nPenyakit ini tersebar diseluruh dunia dan lebih sering ditemukan pada pria dewasa muda.Penyebab penyakit ini adalah actinomyces israelii atau actinomyces bovis yang bersifat anaerob, hidup komensal di dalam rongga mulut atau mukosa faring dan laring. jamur ini berkembang subur pada orang dengan higiene buruk\r\nDari rongga mulut penyakit ini dapat berkembang kearah rahang, kesaluran cerna, paru, dan akhirnya kekulit.\r\n','aktinomikosis.jpg','larutan kalium iodida jenuh dengan dosis 3x50 tetes per hari\r\nItrakonasol dengan dosis 2x100 mg/hari diberikan dengan teratur selama 2-3 bulan.\r\n');

/*Table structure for table `relasi` */

DROP TABLE IF EXISTS `relasi`;

CREATE TABLE `relasi` (
  `kd_penyakit` char(4) NOT NULL,
  `kd_gejala` char(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `relasi` */

insert  into `relasi`(`kd_penyakit`,`kd_gejala`) values ('P002','G001'),('P002','G002'),('P002','G009'),('P002','G014'),('P002','G015'),('P002','G032'),('P002','G034'),('P002','G035'),('P003','G001'),('P003','G002'),('P003','G025'),('P004','G001'),('P004','G003'),('P004','G006'),('P005','G001'),('P005','G004'),('P005','G005'),('P006','G001'),('P006','G008'),('P006','G012'),('P006','G021'),('P006','G022'),('P006','G024'),('P006','G032'),('P006','G038'),('P007','G001'),('P007','G010'),('P007','G014'),('P008','G001'),('P008','G013'),('P008','G014'),('P008','G015'),('P008','G025'),('P008','G036'),('P008','G043'),('P009','G002'),('P009','G011'),('P009','G014'),('P009','G023'),('P009','G027'),('P009','G028'),('P009','G029'),('P009','G030'),('P009','G043'),('P010','G002'),('P010','G017'),('P010','G018'),('P010','G019'),('P010','G041'),('P011','G002'),('P011','G017'),('P011','G019'),('P011','G041'),('P011','G043'),('P002','G002'),('P013','G007'),('P013','G020'),('P013','G037'),('P013','G040'),('P013','G041');

/*Table structure for table `tmp_analisa` */

DROP TABLE IF EXISTS `tmp_analisa`;

CREATE TABLE `tmp_analisa` (
  `kodeauto` varchar(60) NOT NULL DEFAULT '',
  `kd_penyakit` char(4) NOT NULL DEFAULT '',
  `kd_gejala` char(4) NOT NULL DEFAULT '',
  `status` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tmp_analisa` */

/*Table structure for table `tmp_gejala` */

DROP TABLE IF EXISTS `tmp_gejala`;

CREATE TABLE `tmp_gejala` (
  `kd_gejala` char(4) NOT NULL,
  `kodeauto` varchar(60) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tmp_gejala` */

insert  into `tmp_gejala`(`kd_gejala`,`kodeauto`) values ('G002','eIuC9eo7aT'),('G001','eIuC9eo7aT'),('G014','hAt0XmHMZV'),('G014','eLjpF2rzYL'),('G026','VBxJMLVCKk'),('G001','hAt0XmHMZV'),('G013','hAt0XmHMZV');

/*Table structure for table `tmp_pasien` */

DROP TABLE IF EXISTS `tmp_pasien`;

CREATE TABLE `tmp_pasien` (
  `id` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama` varchar(60) NOT NULL,
  `kelamin` enum('P','W') NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `pekerjaan` varchar(60) NOT NULL,
  `kodeauto` varchar(60) NOT NULL,
  `tanggal` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `tmp_pasien` */

insert  into `tmp_pasien`(`id`,`nama`,`kelamin`,`alamat`,`pekerjaan`,`kodeauto`,`tanggal`) values (0003,'G','P','G','G','VBxJMLVCKk','2018-05-21 05:12:04'),(0004,'G','P','G','G','eLjpF2rzYL','2018-05-21 05:12:29'),(0006,'I','P','I','I','HDpECDnIuk','2018-05-21 05:18:07'),(0007,'I','P','I','I','cX11nsrARO','2018-05-21 05:19:08'),(0008,'Ghani','P','Jakarta','Swasta','hAt0XmHMZV','2018-05-21 17:46:36'),(0010,'Ratu','W','Bekasi','Swasta','eIuC9eo7aT','2018-05-21 17:53:32');

/*Table structure for table `tmp_penyakit` */

DROP TABLE IF EXISTS `tmp_penyakit`;

CREATE TABLE `tmp_penyakit` (
  `kd_penyakit` char(4) NOT NULL,
  `kodeauto` varchar(60) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tmp_penyakit` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
