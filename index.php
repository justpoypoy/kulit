<?php //error_reporting(0); ?>
<html>
<head>
	<title>.::SISTEM PAKAR KULIT::.</title>
	<link rel="shortcut icon" href="images/icon.ico" />
	<link href="css/default.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="css/pop.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript">
		function tutup (bersih) {
			document.getElementById(bersih).innerHTML="";
		}
	</script>
</head>
<body>
<div id="header">
	<div id="logo"><img src="images/logo.png" alt="" title="" width="162" height="54" border="0" /></div>
	<h1>Sistem Pakar Kulit</h1>
</div>
<div id="menu">
	<ul>
		<li class="current_page_item"><a href="index.php"><img src="images/i1.png" height="27">Home</a></li>
		<li><a href="index.php?page=daftarpenyakit"><img src="images/i3.png" height="27">Penyakit</a></li>
		<li><a href="index.php?page=konsultasi"><img src="images/i2.png" height="27">Konsultasi</a></li>
	</ul>
</div>
<div id="box"></div>
<div id="page">
	<div id="content">
		<div class="post">
		<?php
		include "materi.php"
		?>			
		</div>
	</div>
	<div id="sidebar">
		<div id="search">
			<h2>Search</h2>
			<form id="searchform" method="post" action="?page=cari">
				<fieldset>
				<input type="text" name="kata" size="15" />
				<input type="submit" name="Search" value="Search" />
				</fieldset>
			</form>
		</div>
		<ul>
			<li id="Profil">
				<h2>Literatur</h2>
				<ul>
					<?php
						include "data/pro/profil.php"
					?>
				</ul>
			</li>
		</ul>
	</div>
	<div id="extra" style="clear: both;">&nbsp;</div>
</div>
<div id="footer">
	<p class="legal"> &copy;<?= date('Y') ?> Kelompok 3 BSI
</div>
</body>
</html>
